"""
Na podacima iz zadatka 1 potrebno je odrediti parametre linearnog modela na podacima za učenje na način da se
implementira funkcija za izračunavanje parametara linearnog modela prema (4-10). Usporedite dobivene parametre
modela s vrijednostima parametara linearnog modela iz zadatka 1.
"""
import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import mean_squared_error

"""
Potrebno je dodati poseban stupac s jedinicama u xtrain kako bismo mogli dobiti zadovoljavajuć oblik s dva rezultantna parametra.
Zatim pomoću dane formule implementirane u metodi "calculateParams" izračunamo tražene parametre.
"""

def non_func(x):
 y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
 return y

def add_noise(y):
 np.random.seed(14)
 varNoise = np.max(y) - np.min(y)
 y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
 return y_noisy

#funkcija za dodavanje stupca s jedinicama
def addOnes(xtrain):
 column_ones=np.ones(len(xtrain))
 column_ones=column_ones[:,np.newaxis]
 x_p=np.hstack((column_ones,xtrain))
 return x_p


#funkcija za izracun parametara
def calculateParams(x_p):
 transposed=np.transpose(x_p)
 invert=np.linalg.inv(np.dot(transposed,x_p))
 theta1=np.dot(np.dot(invert,transposed),ytrain)
 return theta1


x = np.linspace(1,10,100)
y_true = non_func(x)
y_measured = add_noise(y_true)

np.random.seed(12)
indeksi = np.random.permutation(len(x))
indeksi_train = indeksi[0:int(np.floor(0.7*len(x)))]
indeksi_test = indeksi[int(np.floor(0.7*len(x)))+1:len(x)]

x = x[:, np.newaxis]
y_measured = y_measured[:, np.newaxis]

xtrain = x[indeksi_train]
ytrain = y_measured[indeksi_train]

xtest = x[indeksi_test]
ytest = y_measured[indeksi_test]

linearModel = lm.LinearRegression()
linearModel.fit(xtrain,ytrain)

print ('Model je oblika y_hat = Theta0 + Theta1 * x')
print ('y_hat = ', linearModel.intercept_, '+', linearModel.coef_, '*x')
print('\n')
ytest_p = linearModel.predict(xtest)
MSE_test = mean_squared_error(ytest, ytest_p)

plt.figure(3)
plt.plot(xtest,ytest_p,'og',label='predicted')
plt.plot(xtest,ytest,'or',label='test')
plt.legend(loc = 4)

x_pravac = np.array([1,10])
x_pravac = x_pravac[:, np.newaxis]
y_pravac = linearModel.predict(x_pravac)
plt.plot(x_pravac, y_pravac)
plt.show()

x_p=addOnes(xtrain)

theta1= calculateParams(x_p)
print("parametri dobiveni formulom: " + str(theta1[0]) +" " + str(theta1[1]))
