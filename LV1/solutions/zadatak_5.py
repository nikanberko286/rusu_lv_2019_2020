# Napišite program koji od korisnika zahtijeva unos imena tekstualne datoteke. 
# Program nakon toga treba tražiti linije
# oblika:
# X-DSPAM-Confidence: <neki_broj>
# koje predstavljaju pouzdanost korištenog spam filtra.
# Potrebno je izračunati srednju vrijednost pouzdanosti. Koristite
# datoteke mbox.txt i mbox-short.txt
# Primjer
# Ime datoteke: mbox.txt
# Average X-DSPAM-Confidence: 0.894128046745
# Ime datoteke: mbox-short.txt
# Average X-DSPAM-Confidence: 0.750718518519


from sys import exit

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

data=[]

iterator=0

for line in fhand:
    words = line.split()

    for word in words:
        if word=="X-DSPAM-Confidence:":
            data.append(words[iterator+1])
print(data)
valSum=0.0
for value in data:
    valSum+=float(value)
print("Average confidence: " + str(valSum/len(data)))
