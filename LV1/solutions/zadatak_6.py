  # Napišite Python skriptu koja će učitati tekstualnu
# datoteku te iz redova koji počinju s „From“ izdvojiti mail adresu te ju
# spremiti u listu. Nadalje potrebno je
# napraviti dictionary koji sadrži hostname 
# (dio emaila iza znaka @) te koliko puta se
# pojavljuje svaki hostname u učitanoj datoteci.
# Koristite datoteku mbox-short.txt. Na ekran ispišite samo nekoliko
# email adresa te nekoliko zapisa u dictionary-u. 

from sys import exit

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

emails=[]

iterator=0

for line in fhand:
    words = line.split()
    for word in words:
        if word=="From":
            emails.append(words[iterator+1])

hostnames=[]

for email in emails:
    temp=email.split('@')
    hostnames.append(temp[1])
print(hostnames)

fileData=open(fname,'r').read()


occurences={}
for hostname in hostnames:
    occurences[hostname]=fileData.count(hostname)
print(occurences)    


