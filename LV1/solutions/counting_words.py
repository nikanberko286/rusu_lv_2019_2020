# how many times each word appears in the txt file
from sys import exit
fname = input('Enter the file name: ')  #raw_inpt promijenjen u input
try:
    fhand = open(fname) #preimenovati u fname
except:
    print ('File cannot be opened:', fname) #fale zagrade kod print funkcije
    exit()

counts = dict()
for line in fhand:
    words = line.split()
    for word in words:
        if word not in counts:
            counts[word] = 1 #stvara se key-value par pri pronalasku nove rijeci
                             #te se broj pojavljivanja postavlja na 1 kao znak 
                             #prvog pojavljivanja
        else:
            counts[word] +=1 #promijenjena logika-> svaki puta
                             #kada se pronadje postojeca rijec 
                             #dodaje se 1 kao znak pronalaska jos jednog pojavljivanja

print(counts) #fale zagrade kod print funkcije

# But soft what light through yonder window breaks
# It is the east and Juliet is the sun
# Arise fair sun and kill the envious moon
# Who is already sick and pale with grief
