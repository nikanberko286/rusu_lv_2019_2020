#Napišite program koji od korisnika zahtijeva unos brojeva u
#beskonačnoj petlji sve dok korisnik ne upiše „Done“ (bez
#navodnika). Nakon toga potrebno je ispisati koliko brojeva
#je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Osigurajte program od krivog unosa
#(npr. slovo umjesto brojke) na način da program zanemari taj unos i
#ispiše odgovarajuću poruku.

num= 0
numSum = 0.0

number = input('Number: ')
if number== 'done':
    print('no numbers entered')
else:
    try :
        num1=float(number)
    except:
        print('Enter a number or the "Done" keyword')
        number = input('Number: ')
num=num+1
numSum=numSum + num
largestNum=number
lowestNum=number

while True:
    number = input('Number: ')
    if number== 'done':
        break
    try :
        num1=float(number)
    except:
        print('Enter a number or the "Done" keyword')
        continue
    num=num+1
    numSum=numSum + num1
    if number>largestNum:
        largestNum=number
    if number<lowestNum:
        lowestNum=number
print ('all done')
print (numSum,num,numSum/num, largestNum, lowestNum)
