import numpy as np
from skimage import io
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.utils import shuffle

colors = 10
imgOriginal = io.imread('neka lokalna slika')

imgArray = np.reshape(imgOriginal, (-1, 3))
imgArray = imgArray / 255

imgArrayTrain = shuffle(imgArray, random_state=0)[:10000]
kmeans = KMeans(n_clusters=colors, random_state=0).fit(imgArrayTrain)

labels = kmeans.predict(imgArray)
centers = kmeans.cluster_centers_
img = np.reshape(centers[labels], imgOriginal.shape)

plt.figure(1)
plt.imshow(imgOriginal)

plt.figure(2)
plt.imshow(img)