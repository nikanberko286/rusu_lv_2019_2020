import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

try:
    face = sp.face(gray=True)
except AttributeError:
    from scipy import misc

    face = misc.face(gray=True)

X = face.reshape((-1, 1))
k_means = cluster.KMeans(n_clusters=5, n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
face_compressed = np.choose(labels, values)
face_compressed.shape = face.shape

plt.figure(1)
plt.imshow(face, cmap='gray')

plt.figure(2)
plt.imshow(face_compressed, cmap='gray')

imageNew = mpimg.imread('neka lokalna slika')

X = imageNew.reshape((-1, 1))
k_means = cluster.KMeans(n_clusters=10, n_init=1)
k_means.fit(X)
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageNew_compressed = np.choose(labels, values)
imageNew_compressed.shape = imageNew.shape

plt.figure(3)
plt.imshow(imageNew, cmap='gray')

plt.figure(4)
plt.imshow(imageNew_compressed, cmap='gray')