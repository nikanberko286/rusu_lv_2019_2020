import scipy as sp
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
import math

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)


    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples, centers=4,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

for i in range(1,6):
    #if(i == 3): i = i+1
    X = generate_data(500, i)
    #print(X )
    #prikaz rezultata 
    plt.figure()
    plt.title('podaci')
    plt.scatter(X [:,0], X [:,1])
    plt.xlabel('x_1')
    plt.ylabel('x_2')

    kmeans = KMeans(n_clusters = 3, init = 'k-means++', max_iter = 300, n_init=10, random_state = 0)
    pred_y = kmeans.fit_predict(X)
    #print(kmeans)
    #print(pred_y)
    #dodavanje crvenih točkica
    plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], s=50, c = 'red')
    plt.show()

    #prikaz s istim bojama
    plt.figure()
    plt.scatter(X [:,0], X [:,1], c = pred_y)
    plt.scatter(kmeans.cluster_centers_[:,0], kmeans.cluster_centers_[:,1], s=50, c = 'red')
    plt.show()