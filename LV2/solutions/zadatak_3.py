# Napravite jedan vektor proizvoljne dužine koji sadrži slučajno generirane 
# cjelobrojne vrijednosti 0 ili 1. Neka 1
# označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji 
# sadrži visine osoba koje se dobiju uzorkovanjem
# odgovarajuće distribucije. U slučaju muških osoba neka je to Gaussova distribucija
#  sa srednjom vrijednošću 180 cm i
# standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova
#  distribucija sa srednjom vrijednošću 167 cm
# i standardnom devijacijom 7 cm. Prikažite podatke te ih obojite plavom (1) 
# odnosno crvenom bojom (0). Napišite
# funkciju koja računa srednju vrijednost visine za muškarce odnosno žene 
# (probajte izbjeći for petlju pomoću funkcije
# np.dot). Prikažite i dobivene srednje vrijednosti na grafu. 

import numpy as np
import random
import matplotlib.pyplot as plt

data= np.zeros(10, int)
heights= np.zeros(10)

for i in range(10):
    data[i]=random.randint(0,1)


for i in range(10):
    if(data[i]==1):
        heights[i]=np.random.normal(180,7,1)
    else:
        heights[i]=np.random.normal(167,7,1)


for i in range(10):
    if(data[i]==1):
        plt.scatter(data[i], heights[i], c='red')
    else:
        plt.scatter(data[i], heights[i], c='blue')
        



menAverage=np.dot(data,heights)/np.count_nonzero(data)

tempData=(data-1)*-1

print(data)
print(tempData)

womenAverage=np.dot(tempData,heights)/(np.count_nonzero(tempData))

plt.scatter(1, menAverage, c='black', marker='x')

plt.scatter(0, womenAverage, c='black', marker='x')





