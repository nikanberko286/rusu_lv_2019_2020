# U direktoriju rusu_lv_2019_20/LV2/resources nalazi se datoteka
# mtcars.csv koja sadrži različita
# mjerenja provedena na 32 automobila (modeli 1973-74).
# Prikažite ovisnost potrošnje automobila (mpg) o konjskim
# snagama (hp). Na istom grafu prikažite i informaciju o 
# težini pojedinog vozila. Ispišite minimalne, maksimalne i
# srednje vrijednosti potrošnje automobila. 

import matplotlib.pyplot as plt 
import csv

mpg=[]
hp=[]
weight=[]
car=[]
with open('mtcars.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are: {", ".join(row)}')
            line_count += 1
        else:
            mpg.append(row[1])
            hp.append(row[4])
            weight.append(row[6])
            car.append(row[0])
            line_count += 1
    print(f'Processed {line_count} lines.')

for i in range(32):
    plt.scatter(mpg[i], hp[i], c='black')
    plt.annotate(weight[i], (mpg[i], hp[i]))
    
    #plt.scatter(data[i], heights[i], c='blue')
    
print('minimum mpg: ' + car[mpg.index(min(mpg))] +' '+  min(mpg))
print('maximum mpg: ' + car[mpg.index(max(mpg))] +' '+  max(mpg))
print('Average mpg ' + str(sum([float(x) for x in mpg]) / len(mpg)))

print('\n')
print(mpg)
print('\n')
print(hp)
print('\n')
print(weight)