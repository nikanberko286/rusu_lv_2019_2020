# Na temelju primjera 2.8. učitajte sliku 'tiger.png'. 
# Manipulacijom odgovarajuće numpy matrice pokušajte posvijetliti
# sliku (povećati brightness).

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

img = mpimg.imread('tiger.png')


brighterImg=img*1.9
imgplot = plt.imshow(brighterImg) 