# Napišite Python skriptu koja će učitati tekstualnu datoteku, 
# pronaći valjane email adrese te izdvojiti samo prvi dio
# adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz.
#  Koristite datoteku mbox-short.txt. Ispišite rezultat. 
import re

from sys import exit

fname = input('Enter the file name: ')  
try:
    fhand = open(fname)
except:
    print('File cannot be opened:', fname)
    exit()

emails=[]
iterator=0

for line in fhand:
    words = line.split()
    for word in words:
        if word=="From":
            emails.append(words[iterator+1])
print(emails)

concatEmails=' '.join(emails)
lst = re.findall('([\w\.-]+)@[\w\.-]+\.[\w\-]+', concatEmails)

print(lst) 
