#Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6).
#Pomoću histograma prikažite rezultat ovih bacanja. 

import numpy as np
import random
import matplotlib.pyplot as plt 

data= np.zeros(100)
for i in range(100):
    data[i]=random.randint(1,6)
print(data)

plt.hist(data, bins=50, density=True, stacked=True)