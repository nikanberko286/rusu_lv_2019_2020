# Za mtcars skup podataka (nalazi se rusu_lv_2019_20/LV3/resources) napišite 
# programski kod koji će
# odgovoriti na sljedeća pitanja:
# 1. Kojih 5 automobila ima najveću potrošnju? (koristite funkciju sort)
# 2. Koja tri automobila s 8 cilindara imaju najmanju potrošnju?
# 3. Kolika je srednja potrošnja automobila sa 6 cilindara?
# 4. Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
# 5. Koliko je automobila s ručnim, a koliko s automatskim mjenjačem u ovom skupu podataka?
# 6. Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga?
# 7. Kolika je masa svakog automobila u kilogramima?

import matplotlib.pyplot as plt 
import csv
import numpy as np
import pandas as pd

mtcars = pd.read_csv('mtcars.csv')

mtcarsSort=mtcars.sort_values("mpg", axis = 0, ascending = False,
                   na_position ='last')

#5 automobila s najvećom potrošnjom
print(mtcarsSort.head(5))

print("\n")

mtcarsOnly8=mtcars[mtcars.cyl==8]
mtcarsOnly8Sort=mtcarsOnly8.sort_values("mpg", axis = 0, ascending = False,
                   na_position ='last')

#3 automobila s 8 cilindara koji imaju najmanju potrošnju
print(mtcarsOnly8Sort.tail(3))
print("\n")

#srednja potrošnja automobila s 6 cilindara
mtcarsOnly6=mtcars[mtcars.cyl==6]
print(mtcarsOnly6.mean(axis=0)['mpg'])
print("\n")


#Kolika je srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs?
mtcarsWeightBound= mtcars[(mtcars.wt>=2.0) & (mtcars.wt<=2.2) & (mtcars.cyl==4)]
mtcarsWeightBoundMpgMean= mtcarsWeightBound.qsec.mean(axis=0)
print('\n')

#srednja potrošnja automobila s 4 cilindra mase između 2000 i 2200 lbs
print(mtcarsWeightBoundMpgMean)

print('\n')
mtcarsAutomatic= mtcars[(mtcars.am==0)].count(axis=0)
print(mtcarsAutomatic.count())
print("\n")
print((mtcars.count()-mtcarsAutomatic.count()).count())
print('\n')


#Koliko je automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga? 
am0hp100=mtcars[(mtcars.am==0) & (mtcars.hp>100)].count().car
print(am0hp100)

#Kolika je masa svakog automobila u kilogramima?
mtcars['wtkg']=mtcars.wt/2.205*1000
print(mtcars)
print('\n')


