# Napišite programski kod koji će iscrtati sljedeće slike:
# 1. Pomoću barplot-a prikažite na istoj slici potrošnju automobila s 4, 6 i 8 cilindara.
# 2. Pomoću boxplot-a prikažite na istoj slici distribuciju težine automobila s
# 4, 6 i 8 cilindara.
# 3. Pomoću odgovarajućeg grafa pokušajte odgovoriti na pitanje imaju li automobili
# s ručnim mjenjačem veću
# potrošnju od automobila s automatskim mjenjačem?
# 4. Prikažite na istoj slici odnos ubrzanja i snage automobila za automobile s 
# ručnim odnosno automatskim mjenjačem.

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd   

mtcars = pd.read_csv('mtcars.csv') 
cyl4= mtcars[mtcars.cyl == 4]   
cyl6= mtcars[mtcars.cyl == 6]
cyl8= mtcars[mtcars.cyl == 8]

index1=np.arange(0,len(cyl4),1)
index2=np.arange(0,len(cyl6),1)
index3=np.arange(0,len(cyl8),1)


width = 0.3


plt.figure()

plt.bar(index1, cyl4["mpg"],width, color=(1,0,0))

plt.bar(index2 + width, cyl6["mpg"],width, color=(0,1,0.3))

plt.bar(index3 + 2*width, cyl8["mpg"],width, color=(0,0,1))

plt.title("Potrosnja")
plt.xlabel('auto')
plt.ylabel('mpg')

plt.grid(axis='y',linestyle='--')
plt.legend(['4 cilindra','6 cilindara','8 cilindara'],loc=2)



wt4cyl=[]
wt6cyl=[]
wt8cyl=[]

for i in cyl4["wt"]:
    wt4cyl.append(i)    

for i in cyl6["wt"]:
    wt6cyl.append(i)   

for i in cyl8["wt"]:
    wt8cyl.append(i)

plt.figure()
plt.boxplot([wt4cyl, wt6cyl, wt8cyl], positions = [4,6,8]) 
plt.title("Tezina automobila")
plt.xlabel('Broj cilindara')
plt.ylabel('Tezina')
plt.grid(axis='y',linestyle='--')

automaticCars=mtcars[(mtcars.am== 1)]
mpgCars=[]
for i in automaticCars["mpg"]:
    mpgCars.append(i)
    
manualCars=mtcars[(mtcars.am== 0)]
mpgManual=[]
for i in manualCars["mpg"]:
    mpgManual.append(i)
    
plt.figure()
plt.boxplot([mpgManual, mpgCars], positions = [0,1],sym='k+')
plt.title("Potrosnja")
plt.ylabel('Milje po galonu goriva')
plt.xlabel('0 - rucni mjenjac, 1 - automatski mjenjac')
plt.grid(axis='y',linestyle='--')

qsecAutomatic=[]
hpAutomatic=[]
qsecManual=[]
hpManual=[]

for i in automaticCars["qsec"]:  
    qsecAutomatic.append(i)
    
for i in automaticCars["hp"]:  
    hpAutomatic.append(i)
    
for i in manualCars["qsec"]:  
    qsecManual.append(i)
    
for i in manualCars["hp"]:  
    hpManual.append(i)

plt.figure()
plt.scatter(qsecAutomatic, hpAutomatic, marker='x')#iksici
plt.scatter(qsecManual, hpManual, marker='o', facecolors='none', edgecolors='r') #oksici
plt.title("Ubrzanje")
plt.ylabel('Konjske snage')
plt.xlabel('Ubrzanje')
plt.legend(["Automatski mjenjac","Rucni mjenjac"])
plt.grid()
